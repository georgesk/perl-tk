Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Files-Excluded:
 PNG/zlib/contrib/dotzlib/DotZLib.chm,
 JPEG/jpeg/ltconfig,
 JPEG/jpeg/config.guess,
 JPEG/jpeg/config.sub,
 PNG/libpng/config.guess,
 PNG/libpng/config.sub,
Source: https://github.com/eserte/perl-tk
Upstream-Name: perl-tk
Files:
 *
Copyright: 1995-1996 Nick Ing-Simmons
	   1993-95 Ioi Kim Lam
	   1996 Expert Interface Technologies
License: A-special-License and Tix-License or pTk-License

Files:
 ppport.h
Copyright: 2004-2013, Marcus Holland-Moritz
	   2001, Paul Marquess
	   1999, Kenneth Albanowski
License: Perl-like-License

Files:
 debian/*
Copyright: 1999 Raphael Hertzog
	   2002-2024 Stephen Zander
	   2005-2006 Michael C. Schultheiss
	   2008-2017 Colin Tuckley
	   2022-2023 Georges Khaznadar
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, see &lt;https://www.gnu.org/licenses/&gt;.
Comment:
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file '/usr/share/common-licenses/GPL-2'.
 

License: Perl-like-License
 Perl is distributed under the Artistic and GNU Public licences.
 .
 A copy of the Artistic licence may be found at
 /usr/share/common-licenses/Artistic
 .
 A copy of the GNU Public Licence may be found at
 /usr/share/common-licenses/GPL-2


License: A-special-License
 This package is free software; you can redistribute it and/or
 modify it under the same terms as Perl itself, with the exception
 of the files in the pTk sub-directory which have separate terms
 derived from those of the orignal Tk4.0 sources and/or Tix. 
 .
 IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
 DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
 IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
 NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
 MODIFICATIONS.

License: Tix-License
   The following terms apply to all files associated with the software
   unless explicitly disclaimed in individual files.
 .
  RE-DISTRIBUTION OF THE TIX SOFTWARE IN ELECTRONIC FORMAT(S)
 .
   Permission is hereby granted, without written agreement and without
   license or royalty fees, to use, copy, modify, and distribute this
   software and its documentation, in electronic format(s), for any
   purpose, provided that existing copyright notices are retained in all
   copies and that this notice is included verbatim in any distributions.
  .
   Parts of this software is based on the Tcl/Tk software copyrighted by
   the Regents of the University of California, Sun Microsystems, Inc.,
   and other parties. The original license terms of the Tcl/Tk software
   distribution is included in the file pTk/license.terms.
 .
  RE-DISTRIBUTION OF THE TIX DOCUMENTATION IN PRINTED FORMAT
 .
   Permission is hereby granted, without written agreement and without
   license or royalty fees, to distribute the documentation of this
   software in printed format(s), for any purpose, under the following
   conditions:
    1. Existing copyright notices must be retained in all copies.
    2. This notice must be included verbatim in any distributions.
    3. No fees exceeding the cost of printing the documentation can be
       charged to the recipient of the printed copies.
 .     
  DISCLAIMER OF ALL WARRANTIES
 .
   IN NO EVENT SHALL THE AUTHOR OF THIS SOFTWARE BE LIABLE TO ANY PARTY
   FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
   ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN
   IF THE AUTHOR OF THIS SOFTWARE HAS BEEN ADVISED OF THE POSSIBILITY OF
   SUCH DAMAGE.
 . 
   THE AUTHOR OF THIS SOFTWARE SPECIFICALLY DISCLAIMS ANY WARRANTIES,
   INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
   PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHOR OF THIS
   SOFTWARE HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
   ENHANCEMENTS, OR MODIFICATIONS.

License: pTk-License
 This software is copyrighted by the Regents of the University of
 California, Sun Microsystems, Inc., and other parties.  The following
 terms apply to all files associated with the software unless explicitly
 disclaimed in individual files.
 .
 The authors hereby grant permission to use, copy, modify, distribute,
 and license this software and its documentation for any purpose, provided
 that existing copyright notices are retained in all copies and that this
 notice is included verbatim in any distributions. No written agreement,
 license, or royalty fee is required for any of the authorized uses.
 Modifications to this software may be copyrighted by their authors
 and need not follow the licensing terms described here, provided that
 the new terms are clearly indicated on the first page of each file where
 they apply.
 .
 IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
 DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
 IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
 NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
 MODIFICATIONS.
 .
 RESTRICTED RIGHTS: Use, duplication or disclosure by the government
 is subject to the restrictions as set forth in subparagraph (c) (1) (ii)
 of the Rights in Technical Data and Computer Software Clause as DFARS
 252.227-7013 and FAR 52.227-19.
